import React, { useState, useEffect } from "react";

import { Container, Table } from "reactstrap";
import styled from "styled-components";

const IconoCentrado = styled.i`
  position: absolute;
  left: 0;
  right: 0;
  top: 200px;
  width: 100%;
`;

const CovidCat = () => {
    const [llista, setLlista] = useState([]);
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const codiComarca = "13";
        const apiUrl = `https://analisi.transparenciacatalunya.cat/resource/jvut-jxu8.json?codi=${codiComarca}&residencia=No`;
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => {
                setLlista(data);
                setLoading(false);
            })
            .catch((error) => setError(true));


    }, []);

    if (error) {
        return <h3>Se ha producido un error...</h3>;
    }


    if (loading) {
        return <IconoCentrado className="fa fa-spinner fa-spin fa-2x fa-fw" />;
    }

    const filas = llista.sort((a, b) => {
        const dataA = a.data_ini;
        const dataB = b.data_ini;

        let comparison = 0;
        if (dataA > dataB) {
            comparison = 1;
        } else if (dataA < dataB) {
            comparison = -1;
        }
        return comparison;
    }).filter((el, idx) => idx % 7 === 0 || idx === 0).map((el, idx) => {
        //(el.data_ini).split("T")[0].split("-").reverse().join("-")       
        return (
            <tr key={idx}>
                <td>{el.data_ini.split("T")[0]}</td>
                <td>{el.data_fi.split("T")[0]}</td>
                <td>{el.taxa_casos_confirmat}</td>
                <td>{el.casos_confirmat}</td>
            </tr>
        )
    })

    return (
        <Container>
            <br />
            <br />
            <Table>
                <thead>
                    <tr>
                        <th>Data i</th>
                        <th>Data f</th>
                        <th>Taxa</th>
                        <th>Casos</th>
                    </tr>
                </thead>
                <tbody>
                    {filas}
                </tbody>
            </Table>

        </Container>
    );
};

export default CovidCat;